<?php
//incluye la clase Tareas y CrudTareas
require_once('model/crud_tareas.php');
require_once('model/Tareas.php');
$crud= new CrudTareas();
$tarea= new Tareas();
//obtiene todos los libros con el método mostrar de la clase crud
$listaTareas=$crud->mostrar();
?>
 
<html>
<head>
	<title>Mostrar Tareas</title>
</head>
<body>
	<table>
		<head>
			<td>Tarea</td>
			<td>Fecha</td>
			<td>Descripción</td>
			<td>Duración</td>
			<td>Estado</td>
			<td>Integrante</td>
			<td>Observaciones</td>
			<td>Actualizar</td>
			<td>Eliminar</td>
		</head>
		<body>
			<?php foreach ($listaTareas as $tarea) {?>
			<tr>
				<td><?php echo $tarea->getId_tarea() ?></td>
				<td><?php echo $tarea->getFecha_tarea() ?></td>
				<td><?php echo $tarea->getDesc_tarea()?> </td>
				<td><?php echo $tarea->getDuracion_tarea() ?></td>
				<td><?php echo $tarea->getEstado() ?></td>
				<td><?php echo $tarea->getId_integrante()?> </td>
				<td><?php echo $tarea->getObservaciones()?> </td>
				<td><a href="actualizar.php?id_tarea=<?php echo $tarea->getId_tarea() ?>&accion=a">Actualizar</a> </td>
				<td><a href="tareas_controller.php?id_tarea=<?php echo $tarea->getId_tarea() ?>&accion=e">Eliminar</a>   </td>
			</tr>
			<?php }?>
		</body>
	</table>
	<a href="main.php">Volver</a>
</body>
</html>