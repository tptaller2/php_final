<?php
require_once('../model/crud_tableros.php');
require_once('../model/tableros.php');

$crudTableros = new CrudTableros();
$tableros = new tableros();

$tablero = $crudTableros->obtenerTablero($_GET['id_tablero']);
?>
<html>

<head>
	<meta charset="UTF-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
	<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/waypoints/4.0.0/jquery.waypoints.js"></script>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous" />
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
	<link rel="stylesheet" type="text/css" href="../style.css" />
	<link rel="shortcut icon" href="./img/consulta.ico" type="image/x-icon" />
	<link href="https://fonts.googleapis.com/css2?family=Ubuntu:wght@500&display=swap" rel="stylesheet" />
	<title>Actualizar tarea</title>
</head>

<body>

	<form class="contenedor-tablas col-sm-6" action='../controller/tableros_controller.php' method='post'>
		<input type='hidden' name='id_tablero' value='<?php echo $tablero->getId_tablero()?>'>
		<div class="form-group">
			<label for="mail_usuario">Mail del usuario</label>
			<input input type="email" name="mail_usuario" class="form-control" required>
		</div>
		<input type='hidden' name='compartir' value='compartir'>
		<input class="btn btn-primary" type='submit' value='Compartir'>
		<a class="btn btn-secondary" href="../main.php?id_tablero=<?php echo $_GET['id_tablero'] ?>">Volver</a>
	</form>
</body>

</html>