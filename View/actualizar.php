<?php
//incluye la clase Tarea y crud_tareas
require_once('../model/crud_integrantes.php');
require_once('../model/crud_tareas.php');
require_once('../model/crud_estados.php');
require_once('../model/estados.php');
require_once('../model/integrantes.php');
require_once('../model/Tareas.php');

$crud = new CrudTareas();
$crudIntegrantes = new CrudIntegrantes();
$crudEstados = new CrudEstados();
$tarea = new Tareas();
$estados = new estados();
$integrantes = new integrantes();
$listaIntegrantes = $crudIntegrantes->mostrarFiltrado($_GET['id_tablero']);
$listaEstados = $crudEstados->mostrarFiltrado($_GET['id_tablero']);
$listaTareas = $crud->mostrar();


//busca la tarea utilizando el id, que es enviado por GET desde la vista mostrar.php
$tarea = $crud->obtenerTarea($_GET['id_tarea']);
?>
<html>

<head>
	<meta charset="UTF-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
	<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/waypoints/4.0.0/jquery.waypoints.js"></script>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous" />
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
	<link rel="stylesheet" type="text/css" href="../style.css" />
	<link rel="shortcut icon" href="./img/consulta.ico" type="image/x-icon" />
	<link href="https://fonts.googleapis.com/css2?family=Ubuntu:wght@500&display=swap" rel="stylesheet" />
	<title>Actualizar tarea</title>
</head>

<body>

	<form class="contenedor-tablas col-sm-6" action='../controller/tareas_controller.php' method='post'>
		<input type='hidden' name='id_tarea' value='<?php echo $tarea->getId_tarea() ?>'>
		<div class="form-group">
			<label for="fecha_tarea">Fecha Tarea</label>
			<input input type="date" name="fecha_tarea" class="form-control" required value='<?php echo $tarea->getFecha_tarea() ?>'>
		</div>
		<div class="form-group">
			<label for="duracion_tarea">Duración</label>
			<input input type="number" name="duracion_tarea" class="form-control" value='<?php echo $tarea->getDuracion_tarea() ?>' required>
		</div>
		<div class="form-group">
			<label for="desc_tarea">Descripción</label>
			<input input type="text" name="desc_tarea" class="form-control" value='<?php echo $tarea->getDesc_tarea() ?>' required>
		</div>
		<div class="form-group">
			<label for="descripcion">Integrante</label>
			<select class="form-control" name="id_integrante">
			<option value="#">Sin integrante</option>
			<?php foreach ($listaIntegrantes as $integrantes) { ?>
					<?php if ($integrantes->getId_integrante() === $tarea->getId_integrante()) { ?>
						<option selected value="<?php echo $integrantes->getId_integrante() ?>"><?php echo ($integrantes->getNombre() . " " . $integrantes->getApellido()) ?></option>
					<?php } else { ?>
						<option value="<?php echo $integrantes->getId_integrante() ?>"><?php echo ($integrantes->getNombre() . " " . $integrantes->getApellido()) ?></option>
				<?php }
				} ?>
			</select>
		</div>
		<div class="form-group">
			<label for="estado">Estado</label>
			<select class="form-control" name="estado">
				<option value="#">Sin estado</option>
				<?php foreach ($listaEstados as $estados) { ?>
					<?php if ($estados->getId_estado() === $tarea->getEstado()) { ?>
						<option selected value="<?php echo $estados->getId_estado() ?>"><?php echo ($estados->getDescripcion()) ?></option>
					<?php } else { ?>
						<option value="<?php echo $estados->getId_estado() ?>"><?php echo ($estados->getDescripcion()) ?></option>
				<?php }
				} ?>
			</select>
		</div>
		<div class="form-group">
			<label for="observaciones">Observaciones</label>
			<input class="form-control"type='text' name='observaciones' value='<?php echo $tarea->getObservaciones() ?>'></td>
		</div>

		<input type="hidden" name="id_tablero" value=<?php echo $_GET['id_tablero'] ?> />
		<input type='hidden' name='actualizar' value='actualizar'>
		<input class="btn btn-primary" type='submit' value='Guardar'>
		<a class="btn btn-secondary" href="../main.php?id_tablero=<?php echo $_GET['id_tablero'] ?>">Volver</a>
	</form>
</body>

</html>