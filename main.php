<?php
require_once('model/crud_integrantes.php');
require_once('model/crud_tareas.php');
require_once('model/crud_estados.php');
require_once('model/crud_tableros.php');
require_once('model/estados.php');
require_once('model/integrantes.php');
require_once('model/Tareas.php');
require_once('model/tableros.php');
$user_id = $_COOKIE["user_id"];
$crud = new CrudTareas();
$crudIntegrantes = new CrudIntegrantes();
$crudEstados = new CrudEstados();
$crudTableros = new CrudTableros();
$tarea = new Tareas();
$estados = new estados();
$integrantes = new integrantes();
$tableros = new tableros();
if ($user_id != null) {
  $listaTableros = $crudTableros->mostrarFiltrado($user_id);
} else {
  echo '<script type="text/javascript">alert("Por favor logueate de nuevo. El sitio usa cookies.");
  window.location.href="index.php"</script>';
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css" />
  <link rel="stylesheet" href="https://cdn.datatables.net/1.10.21/css/dataTables.bootstrap4.min.css" />
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous" />
  <link rel="stylesheet" type="text/css" href="style.css" />
  <link rel="shortcut icon" href="./img/consulta.ico" type="image/x-icon" />
  <link href="https://fonts.googleapis.com/css2?family=Ubuntu:wght@500&display=swap" rel="stylesheet" />

  <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
  <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/waypoints/4.0.0/jquery.waypoints.js"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
  <script src="https://cdn.datatables.net/1.10.21/js/dataTables.bootstrap4.min.js" crossorigin="anonymous"></script>
  <script type="text/javascript" src="./datatables.min.js"></script>
  <title>Task Management Tool</title>
</head>

<body data-spy="scroll" data-target="#info-nav" class="d-flex flex-column min-vh-100">
  <nav class="navbar navbar-expand-lg navbar-dark fixed-top">
    <a class="navbar-brand" href="main.php">Task Management Tool</a>
    <button class="btn btn-danger" onclick="logout()">Logout</button>
  </nav>
  <div class="container-fluid">
    <div class="row d-flex justify-content-center introduccion">
      <div class="row" id="home">
        <div class="col-12 fondo-titulo">
          <div class="titulo">Task Management Tool v2.0</div>
        </div>
      </div>
      <div class="col-12">
        <div class="col-md-8 col-lg-7 texto-intro ml-2 mr-2 mt-5 slide-animate-left primer-linea">
          Bienvenido a la herramienta de tareas TMT<br />
          Desde aquí podrá dar seguimiento a las tareas necesarias para llevar
          a cabo su proyecto
          <br /><br />
        </div>
      </div>
    </div>

    <!-- Seccion Control de Proyectos -->
    <div id="controlProyectos">
      <div id="projIntro">
        <div class="col-12 d-flex justify-content-around mt-5 align-items-center">
          <h1>Control de Proyectos</h1>
        </div>

        <div class="row">
          <div class="col-sm-12 col-lg-6 d-flex align-items-center">
            <form class="contenedor-tablas col-sm-8" action="" method="get" id="elegirProyecto">
              <div class="form-group">
                <label for="descripcion">Elija Proyecto para Gestionar Tareas:</label>
                <select class="form-control" id="tablero-select" name="id_tablero" onchange="this.form.submit()">
                  <option disabled selected>Elija un Proyecto: </option>
                  <?php foreach ($listaTableros as $tableros) { ?>
                    <option value="<?php echo $tableros->getId_tablero() ?>"><?php echo ($tableros->getNombreTablero()) ?></option>
                  <?php } ?>
                </select>
              </div>
            </form>
          </div>

          <div class="col-sm-12 col-lg-6 d-flex align-items-center">
            <button class="btn-lg btn-danger mx-auto full-width" onclick="showSection('editProjects')">
              Edición de Proyectos
            </button>
          </div>
        </div>
      </div>

      <section class="contenedor-tablas col-sm-10 table-responsive" id="verProyectos" style="display: none;">
        <table class="justify-content-center table table-striped table-sm table-bordered table-dark table-hover" cellspacing="0" width="100%">
          <thead>
            <th scope="col">Nombre Proyecto</th>
            <th scope="col">Actualizar Proyecto</th>
            <th scope="col">Compartir Proyecto</th>
          </thead>
          <tbody>
            <?php foreach ($listaTableros as $tableros) { ?>
              <tr>
                <td><?php echo $tableros->getNombreTablero() ?></td>
                <td><a class="btn btn-outline-primary" href="view/actualizar_tableros.php?id_tablero=<?php echo $tableros->getId_tablero() ?>&accion=a">Actualizar</a> </td>
                <td><a class="btn btn-outline-light" href="view/compartir_tableros.php?id_tablero=<?php echo $tableros->getId_tablero() ?>&accion=c">Compartir</a> </td>
              </tr>
            <?php } ?>
          </tbody>
        </table>
        <div class="row align-items-center">
          <div class="col">
            <button class="btn-lg btn-secondary full-width" onclick="reloadPage()">
              Volver
            </button>
            <button class="btn-lg btn-primary full-width" onclick="showForm('newProject')">
              Crear nuevo Proyecto
            </button>
          </div>
        </div>
      </section>

      <div id="newProjectForm" class="col-12 justify-content-around mt-3 align-items-center" style="display: none">
        <form class="contenedor-tablas col-sm-6" action="controller/tableros_controller.php" method="post">
          <div class="form-group">
            <label for="descripcion">Nombre del Nuevo Proyecto:</label>
            <input input type="text" name="nombre-proyecto" class="form-control" required>
          </div>
          <input type="hidden" name="insertar" value="insertar" />
          <button class="btn btn-secondary full-width" onclick="showSection('editProjects')">
            Volver
          </button>
          <input class="btn btn-primary" type="submit" value="Guardar" />
          <input type="hidden" name="user_id" value=<?php echo $user_id ?> />
        </form>
      </div>
    </div>

    <!-- Seccion Panel de Control -->
    <div id="panelDeControl" style="display: none">
      <div class="col-12 d-flex justify-content-around panel-home align-items-center">
        <h1>Panel de Control</h1>
      </div>
      <?php $tablero = $crudTableros->obtenerTablero($_GET['id_tablero']); ?>
      <h2>Proyecto Seleccionado: <?php echo $tablero->getNombreTablero() ?></h2>
      <button class="btn-lg btn-danger full-width" onclick="resetPage()">
        Cambiar de Proyecto
      </button>
      <div class="panel-botones row">
        <div class="col-sm-4 d-flex align-items-center">
          <button class="btn-lg btn-primary full-width" onclick="showSection('board')">
            Ver tareas
          </button>
        </div>
        <div class="col-sm-4 d-flex align-items-center">
          <button class="btn-lg btn-primary full-width" onclick="showSection('member')">
            Ver integrantes
          </button>
        </div>
        <div class="col-sm-4 d-flex align-items-center">
          <button class="btn-lg btn-primary full-width" onclick="showSection('status')">
            Ver estados
          </button>
        </div>
      </div>

      <!-- CONTENEDOR DINAMICO -->

      <section class="row" id="dynamic-container">

        <!-- SECCION VER INTEGRANTES -->
        <section class="contenedor-tablas col-sm-10 table-responsive" id="verIntegrantes" style="display: none;" style="display: none;">
          <table id="tableverIntegrantes" class="justify-content-center table table-striped table-sm table-bordered table-dark table-hover" cellspacing="0" width="100%">
            <thead>
              <th scope="col">Nombre </th>
              <th scope="col">Apellido</th>
              <th scope="col">Mail</th>
              <th scope="col">Actualizar Integrante</th>
              <th scope="col">Eliminar Integrante</th>
            </thead>
            <tbody>
              <?php $listaIntegrantes = $crudIntegrantes->mostrarFiltrado($_GET['id_tablero']); ?>
              <?php foreach ($listaIntegrantes as $integrantes) { ?>
                <tr>
                  <td><?php echo $integrantes->getNombre() ?></td>
                  <td><?php echo $integrantes->getApellido() ?> </td>
                  <td><?php echo $integrantes->getMail() ?></td>
                  <td><a class="btn btn-outline-primary" href="view/actualizar_integrantes.php?id_integrante=<?php echo $integrantes->getId_integrante() ?>&accion=a&id_tablero=<?php echo $_GET['id_tablero'] ?>">Actualizar</a> </td>
                  <td><a class="btn btn-outline-danger" href="controller/integrantes_controller.php?id_integrante=<?php echo $integrantes->getId_integrante() ?>&accion=e&id_tablero=<?php echo $_GET['id_tablero'] ?>">Eliminar</a> </td>
                </tr>
              <?php } ?>
            </tbody>
          </table>
          <div class="row align-items-center">
            <div class="col">
              <button class="btn-lg btn-secondary full-width" onclick="reloadPage()">
                Volver
              </button>
              <button class="btn-lg btn-primary full-width" onclick="showForm('member')">
                Ingresar integrante
              </button>
            </div>
          </div>
        </section>

        <form class="contenedor-tablas col-sm-6 " action="controller/integrantes_controller.php" method="post" id="insertarIntegrantesForm" style="display: none;">
          <div class="form-group">
            <label for="nombre">Nombre</label>
            <input input type="text" name="nombre" class="form-control" required>
          </div>
          <div class="form-group">
            <label for="apellido">Apellido</label>
            <input input type="text" name="apellido" class="form-control" required>
          </div>
          <div class="form-group">
            <label for="mail">Mail</label>
            <input input type="email" name="mail" class="form-control" required>
          </div>

          <!--       deberia cambiarse el value de "id_tablero" dinamicamente -->

          <input type="hidden" name="id_tablero" value=<?php echo $_GET['id_tablero'] ?> />
          <input type="hidden" name="insertar" value="insertar" />
          <button class="btn btn-secondary full-width" onclick="showSection('member')">
            Volver
          </button>
          <input type="submit" class="btn btn-primary" value="Guardar" />
        </form>

        <!-- FIN SECCION VER INTEGRANTES -->

        <!-- SECCION VER ESTADOS -->

        <section class="contenedor-tablas col-sm-10 table-responsive" id="verEstados" style="display: none;" style="display: none;">
          <table id="tableverEstados" class="justify-content-center table table-striped table-sm table-bordered table-dark table-hover" cellspacing="0" width="100%">
            <thead>
              <th scope="col">Nombre </th>
              <th scope="col">Actualizar Estado</th>
              <th scope="col">Eliminar Estado</th>
            </thead>
            <tbody>
              <?php $listaEstados = $crudEstados->mostrarFiltrado($_GET['id_tablero']); ?>
              <?php foreach ($listaEstados as $estados) { ?>
                <tr>
                  <td><?php echo $estados->getDescripcion() ?></td>
                  <td><a class="btn btn-outline-primary" href="view/actualizar_estado.php?id_estado=<?php echo $estados->getId_estado() ?>&accion=a&id_tablero=<?php echo $_GET['id_tablero'] ?>">Actualizar</a> </td>
                  <td><a class="btn btn-outline-danger" href="controller/estados_controller.php?id_estado=<?php echo $estados->getId_estado() ?>&accion=e&id_tablero=<?php echo $_GET['id_tablero'] ?>">Eliminar</a> </td>
                </tr>
              <?php } ?>
            </tbody>
          </table>
          <div class="row align-items-center">
            <div class="col">
              <button class="btn-lg btn-secondary full-width" onclick="reloadPage()">
                Volver
              </button>
              <button class="btn-lg btn-primary full-width" onclick="showForm('status')">
                Ingresar estado
              </button>
            </div>
          </div>
        </section>


        <form class="contenedor-tablas col-sm-6" action="controller/estados_controller.php" method="post" id="insertarEstadoForm" style="display: none;">
          <div class="form-group">
            <label for="descripcion">Estado</label>
            <input input type="text" name="descripcion" class="form-control" required>
          </div>

          <!--       deberia cambiarse el value de "id_tablero" dinamicamente -->

          <input type="hidden" name="id_tablero" value=<?php echo $_GET['id_tablero'] ?> />
          <input type="hidden" name="insertar" value="insertar" />
          <button class="btn btn-secondary full-width" onclick="showSection('status')">
            Volver
          </button>
          <input class="btn btn-primary" type="submit" value="Guardar" />
        </form>

        <!-- FIN SECCION VER ESTADOS -->

        <!-- SECCION VER TAREAS -->

        <section class="contenedor-tablas col-sm-10 table-responsive" id="verTareas" style="display: none;">
          <table id="tableverTareas" class="justify-content-center table table-striped table-sm table-bordered table-dark table-hover" cellspacing="0" width="100%">
            <thead>
              <th scope="col">Fecha</th>
              <th scope="col">Descripción</th>
              <th scope="col">Duración</th>
              <th scope="col">Estado</th>
              <th scope="col">Integrante</th>
              <th scope="col">Observaciones</th>
              <th scope="col">Actualizar</th>
              <th scope="col">Eliminar</th>
            </thead>
            <tbody>
              <?php $listaTareas = $crud->mostrarFiltrado($_GET['id_tablero']); ?>
              <?php foreach ($listaTareas as $tarea) { ?>
                <tr>
                  <td><?php echo $tarea->getFecha_tarea() ?></td>
                  <td><?php echo $tarea->getDesc_tarea() ?> </td>
                  <td><?php echo $tarea->getDuracion_tarea() ?></td>
                  <td><?php foreach ($listaEstados as $estado) {
                        if ($estado->getId_estado() === $tarea->getEstado()) { ?>
                        <?php echo $estado->getDescripcion() ?>
                    <?php }
                      } ?></td>
                  <td><?php foreach ($listaIntegrantes as $integrante) {
                        if ($integrante->getId_integrante() === $tarea->getId_integrante()) { ?>
                        <?php echo ($integrante->getNombre() . " " . $integrante->getApellido()) ?>
                    <?php }
                      } ?></td>
                  <td><?php echo $tarea->getObservaciones() ?> </td>
                  <td><a class="btn btn-outline-primary" href="view/actualizar.php?id_tarea=<?php echo $tarea->getId_tarea() ?>&accion=a&id_tablero=<?php echo $_GET['id_tablero'] ?>">Actualizar</a> </td>
                  <td><a class="btn btn-outline-danger" href="controller/tareas_controller.php?id_tarea=<?php echo $tarea->getId_tarea() ?>&accion=e&id_tablero=<?php echo $_GET['id_tablero'] ?>">Eliminar</a> </td>
                </tr>
              <?php } ?>
            </tbody>
          </table>
          <div class="row align-items-center">
            <div class="col">
              <button class="btn-lg btn-secondary full-width" onclick="reloadPage()">
                Volver
              </button>
              <button class="btn-lg btn-primary full-width" onclick="showForm('tasks')">
                Ingresar tarea
              </button>
            </div>
          </div>
        </section>


        <form class="contenedor-tablas col-sm-6" action='controller/tareas_controller.php' method='post' id="insertarTareaForm" style="display: none;">
          <div class="form-group">
            <label for="fecha_tarea">Fecha Tarea</label>
            <input input type="date" name="fecha_tarea" class="form-control" required>
          </div>
          <div class="form-group">
            <label for="duracion_tarea">Duración</label>
            <input input type="number" name="duracion_tarea" class="form-control" required>
          </div>
          <div class="form-group">
            <label for="desc_tarea">Descripción</label>
            <input input type="text" name="desc_tarea" class="form-control" required>
          </div>
          <div class="form-group">
            <label for="descripcion">Integrante</label>
            <select class="form-control" name="id_integrante">
              <option value="#">Sin integrante</option>
              <?php foreach ($listaIntegrantes as $integrantes) { ?>
                <option value="<?php echo $integrantes->getId_integrante() ?>"><?php echo ($integrantes->getNombre() . " " . $integrantes->getApellido()) ?></option>
              <?php } ?>
            </select>
          </div>
          <div class="form-group">
            <label for="estado">Estado</label>
            <select class="form-control" name="estado">
              <option value="#">Sin estado</option>
              <?php foreach ($listaEstados as $estados) { ?>
                <option value="<?php echo $estados->getId_estado() ?>"><?php echo ($estados->getDescripcion()) ?></option>
              <?php } ?>
            </select>
          </div>
          <div class="form-group">
            <label for="observaciones">Observaciones</label>
            <input input type="text" name="observaciones" class="form-control">
          </div>

          <!--       deberia cambiarse el value de "id_tablero" dinamicamente -->

          <input type="hidden" name="id_tablero" value=<?php echo $_GET['id_tablero'] ?> />
          <input type='hidden' name='insertar' value='insertar'>
          <button class="btn btn-secondary full-width" onclick="showSection('board')">
            Volver
          </button>
          <input class="btn btn-primary" type='submit' value='Guardar'>
        </form>

        <!-- FIN SECCION VER TAREAS -->

      </section>
      <!-- FIN CONTENEDOR DINAMICO -->
    </div>


  </div>

  <footer class="pt-3 pl-3">
    Maceira/Ruiz/Salinas/Trillo - 2020
  </footer>

  <script>
    document.addEventListener('DOMContentLoaded', function() {

      const queryString = window.location.search;
      const urlParams = new URLSearchParams(queryString);
      const tablero = urlParams.get('id_tablero');
      let panelDeControl = document.getElementById("panelDeControl");
      let controlProyectos = document.getElementById("controlProyectos");
      if (tablero !== null) {
        $('#tablero-select').val(tablero);
        controlProyectos.style.display = "none";
        panelDeControl.style.display = "block";
      }


    }, false);

    function logout() {
      document.cookie = "user_id=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;";
      alert("Gracias por usar TMT!")
      window.location.href = 'index.php'
    }

    $(".home").waypoint(
      function() {
        $(".home").addClass("slide-animate");
      }, {
        offset: "50%"
      }
    );

    function showSection(selection) {
      let memberBlock = document.getElementById("verIntegrantes");
      let statusBlock = document.getElementById("verEstados");
      let board = document.getElementById("verTareas");
      let introProyectos = document.getElementById("projIntro");
      let tablaProyectos = document.getElementById("verProyectos");

      let boardForm = document.getElementById("insertarTareaForm");
      let memberForm = document.getElementById("insertarIntegrantesForm");
      let statusForm = document.getElementById("insertarEstadoForm");
      let projectForm = document.getElementById("newProjectForm");

      memberForm.style.display = "none";
      statusForm.style.display = "none";
      boardForm.style.display = "none";
      projectForm.style.display = "none";


      if (selection === "member") {
        statusBlock.style.display = "none";
        board.style.display = "none";
        if (memberBlock.style.display === "none") {
          memberBlock.style.display = "block";
        } else {
          memberBlock.style.display = "none";
        }
      }
      if (selection === "status") {
        memberBlock.style.display = "none";
        board.style.display = "none";
        if (statusBlock.style.display === "none") {
          statusBlock.style.display = "block";
        } else {
          statusBlock.style.display = "none";
        }
      }
      if (selection === "board") {
        memberBlock.style.display = "none";
        statusBlock.style.display = "none";
        if (board.style.display === "none") {
          board.style.display = "block";
        } else {
          board.style.display = "none";
        }
      }
      if (selection === "editProjects") {
        introProyectos.style.display = "none";
        tablaProyectos.style.display = "block";
      }
    }

    function showForm(selection) {
      let taskForm = document.getElementById("insertarTareaForm");
      let memberForm = document.getElementById("insertarIntegrantesForm");
      let statusForm = document.getElementById("insertarEstadoForm");
      let projectForm = document.getElementById("newProjectForm");

      if (selection === "tasks") {
        let taskSection = document.getElementById("verTareas");
        taskSection.style.display = "none";
        if (taskForm.style.display === "none") {
          taskForm.style.display = "block";
        } else {
          taskForm.style.display = "none";
        }
      }
      if (selection === "member") {
        let memberSection = document.getElementById("verIntegrantes");
        memberSection.style.display = "none";
        if (memberForm.style.display === "none") {
          memberForm.style.display = "block";
        } else {
          memberForm.style.display = "none";
        }
      }
      if (selection === "status") {
        let statusSection = document.getElementById("verEstados");
        statusSection.style.display = "none";
        if (statusForm.style.display === "none") {
          statusForm.style.display = "block";
        } else {
          statusForm.style.display = "none";
        }
      }
      if (selection === "newProject") {
        let seeProjectsSection = document.getElementById("verProyectos");
        seeProjectsSection.style.display = "none";
        if (projectForm.style.display === "none") {
          projectForm.style.display = "block";
        } else {
          projectForm.style.display = "none";
        }
      }
    }

    function resetPage() {
      window.location.href = 'main.php'
    }

    function reloadPage() {
      window.location.reload();
    }


    function eligioProyecto(sel) {
      let nombreProyectoSection = document.getElementById("proyectoName");
      nombreProyectoSection.innerHTML = "Proyecto Seleccionado: " + sel.options[sel.selectedIndex].text;
    }

    $(document).ready(function() {
      $('#tableverIntegrantes').DataTable();
    });

    $(document).ready(function() {
      $('#tableverEstados').DataTable();
    });

    $(document).ready(function() {
      $('#tableverTareas').DataTable();
    });
  </script>
</body>

</html>