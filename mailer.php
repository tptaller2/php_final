<?php

require_once('model/crud_integrantes.php');
require_once('model/crud_tareas.php');
require_once('model/integrantes.php');
require_once('model/Tareas.php');

function mailer(tareas $tarea)
{

  $crudIntegrantes = new CrudIntegrantes();
  $integrante = new integrantes();

  $id_integrante = $tarea->getId_integrante();

  $integrante = $crudIntegrantes->obtenerIntegrante($id_integrante);

  $tarea_id = $tarea->getId_tarea();
  $desc = $tarea->getDesc_tarea();

  $mail_usuario = $integrante->getMail();
  $nombre_usuario = $integrante->getNombre();

  $nombre = "TASK MANAGEMENT TOOL";
  $fromCorreo = "noreply@taskmanagementtool.com";
  $asunto = "La tarea " . $tarea_id . " - " . $desc . " te fue asignada";
  $mensaje = "Hola " . $nombre_usuario . "! <br> Te asignaron una tarea en TASK MANAGEMENT TOOL. <br> Logueate para revisarla! 
  <br><br>  
" . $tarea->getId_tarea() . " - " . $tarea->getDesc_tarea() . " <br> 
  Inicio: " . $tarea->getFecha_tarea() . "<br>
  Duracion: " . $tarea->getDuracion_tarea() . "<br><br> <h4>TASK MANAGEMENT TOOL team</h4> <br><br>
  URL DE LA PAGINA";

  $mensaje = wordwrap($mensaje, 70, "\r\n");

  $headers = "From: $nombre <$fromCorreo>" . "\r\n" .
    "Reply-To: $nombre <$fromCorreo>" . "\r\n" .
    'X-Mailer: PHP/' . phpversion() . "\r\n" .
    'Content-type: text/html;' . "\r\n";


  mail($mail_usuario, $asunto, $mensaje, $headers);
}
