<?php
	class estados{
		private $id_estado;
		private $descripcion;
		private $id_tablero;

		function __construct(){}

		public function getId_estado(){
		return $this->id_estado;
		}

		public function setId_estado($id_estado){
			$this->id_estado = $id_estado;
		}

		public function getDescripcion(){
			return $this->descripcion;
		}

		public function setDescripcion($descripcion){
			$this->descripcion = $descripcion;
		}

		public function getId_tablero(){
			return $this->id_tablero;
		}

		public function setId_tablero($id_tablero){
			$this->id_tablero = $id_tablero;
		}

	}
?>