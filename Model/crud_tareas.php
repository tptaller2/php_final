<?php
// incluye la clase Db
require_once('conexion.php');
if(file_exists('mailer.php')){
  require_once('mailer.php');
}
if(file_exists('../mailer.php')){
  require_once('../mailer.php');
}

class CrudTareas
{
	// constructor de la clase
	public function __construct()
	{
	}

	// método para insertar, recibe como parámetro un objeto de tipo tareas
	public function insertar($tareas)
	{
		$db = Db::conectar();
		$insert = $db->prepare('INSERT INTO tareas values (NULL,:fecha_tarea,:desc_tarea,:duracion_tarea,:estado,:id_integrante,:observaciones)');
		$insert->bindValue('fecha_tarea', $tareas->getFecha_tarea());
		$insert->bindValue('desc_tarea', $tareas->getDesc_tarea());
		$insert->bindValue('duracion_tarea', $tareas->getDuracion_tarea());
		$insert->bindValue('estado', $tareas->getEstado());
		$insert->bindValue('id_integrante', $tareas->getId_integrante());
		$insert->bindValue('observaciones', $tareas->getObservaciones());
		$insert->execute();

		$tareas->setId_tarea($db->lastInsertId());

		$insert = $db->prepare('INSERT INTO tarea_por_tablero values 
		(NULL,:id_tablero,:id_tarea)');
		$insert->bindValue('id_tablero', $tareas->getId_tablero());
		$insert->bindValue('id_tarea', $tareas->getId_tarea());
		$insert->execute();

		mailer($tareas);
	}

	// método para mostrar todos los tareas
	public function mostrar()
	{
		$db = Db::conectar();
		$listaTarea = [];
		$select = $db->query('SELECT * FROM tareas order by 2, 4, 5');

		foreach ($select->fetchAll() as $tarea) {
			$myTarea = new tareas();
			$myTarea->setId_tarea($tarea['id_tarea']);
			$myTarea->setFecha_tarea($tarea['fecha_tarea']);
			$myTarea->setDesc_tarea($tarea['desc_tarea']);
			$myTarea->setDuracion_tarea($tarea['duracion_tarea']);
			$myTarea->setEstado($tarea['estado']);
			$myTarea->setId_integrante($tarea['id_integrante']);
			$myTarea->setObservaciones($tarea['observaciones']);
			$listaTarea[] = $myTarea;
		}
		return $listaTarea;
	}

	// método para mostrar las tareas filtradas
	public function mostrarFiltrado($id_tablero)
	{
		$db = Db::conectar();
		$listaTarea = [];
		$select = $db->prepare('SELECT * FROM tareas where id_tarea in 
		(select id_tarea from tarea_por_tablero t where t.id_tablero=:id_tablero)
		order by 5, 2, 4');
		$select->bindValue('id_tablero', $id_tablero);
		$select->execute();

		foreach ($select->fetchAll() as $tarea) {
			$myTarea = new tareas();
			$myTarea->setId_tarea($tarea['id_tarea']);
			$myTarea->setFecha_tarea($tarea['fecha_tarea']);
			$myTarea->setDesc_tarea($tarea['desc_tarea']);
			$myTarea->setDuracion_tarea($tarea['duracion_tarea']);
			$myTarea->setEstado($tarea['estado']);
			$myTarea->setId_integrante($tarea['id_integrante']);
			$myTarea->setObservaciones($tarea['observaciones']);
			$listaTarea[] = $myTarea;
		}
		return $listaTarea;
	}

	// método para eliminar un tareas, recibe como parámetro el id del tareas
	public function eliminar($id_tarea)
	{
		$db = Db::conectar();
		$eliminar = $db->prepare('DELETE FROM tarea_por_tablero where id_tarea=:id_tarea'); 
		$eliminar->bindValue('id_tarea', $id_tarea);
		$eliminar->execute();

		$eliminar = $db->prepare('DELETE FROM tareas WHERE id_tarea=:id_tarea'); 
		$eliminar->bindValue('id_tarea', $id_tarea);
		$eliminar->execute();
	}

	// método para buscar un tareas, recibe como parámetro el id del tareas
	public function obtenerTarea($id_tarea)
	{
		$db = Db::conectar();
		$select = $db->prepare('SELECT * FROM tareas WHERE id_tarea=:id_tarea');
		$select->bindValue('id_tarea', $id_tarea);
		$select->execute();
		$tareas = $select->fetch();
		$myTarea = new tareas();
		$myTarea->setId_tarea($tareas['id_tarea']);
		$myTarea->setFecha_tarea($tareas['fecha_tarea']);
		$myTarea->setDesc_tarea($tareas['desc_tarea']);
		$myTarea->setDuracion_tarea($tareas['duracion_tarea']);
		$myTarea->setEstado($tareas['estado']);
		$myTarea->setId_integrante($tareas['id_integrante']);
		$myTarea->setObservaciones($tareas['observaciones']);
		return $myTarea;
	}

	// método para actualizar un tareas, recibe como parámetro el tareas
	public function actualizar($tarea)
	{
		$db = Db::conectar();
		$actualizar = $db->prepare('UPDATE tareas 
			SET fecha_tarea=:fecha_tarea, desc_tarea=:desc_tarea, duracion_tarea=:duracion_tarea, estado=:estado, id_integrante=:id_integrante, observaciones=:observaciones
			WHERE id_tarea=:id_tarea');

		$actualizar->bindValue('id_tarea', $tarea->getId_tarea());
		$actualizar->bindValue('fecha_tarea', $tarea->getFecha_tarea());
		$actualizar->bindValue('desc_tarea', $tarea->getDesc_tarea());
		$actualizar->bindValue('duracion_tarea', $tarea->getDuracion_tarea());
		$actualizar->bindValue('estado', $tarea->getEstado());
		$actualizar->bindValue('id_integrante', $tarea->getId_integrante());
		$actualizar->bindValue('observaciones', $tarea->getObservaciones());
		$actualizar->execute();

		mailer($tarea);
	}
}
