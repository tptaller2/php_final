<?php
	class integrantes{
		private $id_integrante;
		private $nombre;
		private $apellido;
		private $mail;
		private $id_tablero;

		function __construct(){}

		public function getNombre(){
		return $this->nombre;
		}

		public function setNombre($nombre){
			$this->nombre = $nombre;
		}

		public function getApellido(){
			return $this->apellido;
		}

		public function setApellido($apellido){
			$this->apellido = $apellido;
		}

		public function getMail(){
		return $this->mail;
		}

		public function setMail($mail){
			$this->mail = $mail;
		}
		public function getId_integrante(){
			return $this->id_integrante;
		}

		public function setId_integrante($id_integrante){
			$this->id_integrante = $id_integrante;
		}

		public function getId_tablero(){
			return $this->id_tablero;
		}

		public function setId_tablero($id_tablero){
			$this->id_tablero = $id_tablero;
		}
	}
?>