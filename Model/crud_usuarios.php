<?php
// incluye la clase Db
require_once('conexion.php');

class CrudUsuarios
{
	// constructor de la clase
	public function __construct()
	{
	}

	// método para insertar, recibe como parámetro un objeto de tipo libintegrantesro
	public function insertar($usuario)
	{
		$salida = false;

		$db = Db::conectar();

		$select = $db->prepare('SELECT count(*) as CONT from usuario where mail = :mail');
		$select->bindValue('mail', $usuario->getMail());
		$select->execute();
		$contador = $select->fetch();

		if ($contador['CONT'] == 0) {
			$insert = $db->prepare('INSERT INTO usuario values(NULL,:nombre,:apellido,:mail,:password)');
			$insert->bindValue('nombre', $usuario->getNombre());
			$insert->bindValue('apellido', $usuario->getApellido());
			$insert->bindValue('mail', $usuario->getMail());
			$insert->bindValue('password', $usuario->getPassword());
			$insert->execute();

			$salida = true;
		}
		return $salida;
	}

	public function loguear($usuario)
	{
		$salida = false;
		$user = $usuario->getMail();
		$pass = $usuario->getPassword();

		$db = Db::conectar();

		$select = $db->prepare('SELECT COUNT(*) AS CONT, id_user FROM usuario WHERE mail = :mail AND password = :pass');
		$select->bindValue('mail', $usuario->getMail());
		$select->bindValue('pass', $usuario->getPassword());
		$select->execute();
		$contador = $select->fetch();

		if ($contador['CONT'] > 0) {
			setcookie("user_id", $contador['id_user'], time() + 10000, '/');
			$salida = true;
		}

		return $salida;
	}
}

?>