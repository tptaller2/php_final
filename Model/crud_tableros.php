<?php
// incluye la clase Db
require_once('conexion.php');

class CrudTableros
{
	// constructor de la clase
	public function __construct()
	{
	}

	// método para insertar, recibe como parámetro un objeto de tipo tablero
	public function insertar($tablero, $user_id)
	{
		$db = Db::conectar();
		$insert = $db->prepare('INSERT INTO tablero values(NULL,:nombre_tablero)');
		$insert->bindValue('nombre_tablero', $tablero->getNombreTablero());
		$insert->execute();

		$id_tablero = $db->lastInsertId();

		$insert = $db->prepare('INSERT INTO tablero_por_usuario values(NULL,:id_tablero,:id_usuario)');
		$insert->bindValue('id_tablero', $id_tablero);
		$insert->bindValue('id_usuario', $user_id);
		$insert->execute();
	}

	// método para mostrar todos los tableros
	public function mostrar()
	{
		$db = Db::conectar();
		$listaTableros = [];
		$select = $db->query('SELECT * FROM tablero');

		foreach ($select->fetchAll() as $tableros) {
			$myTablero = new tableros();
			$myTablero->setId_tablero($tableros['id_tablero']);
			$myTablero->setNombreTablero($tableros['nombre_tablero']);
			$listaTableros[] = $myTablero;
		}
		return $listaTableros;
	}

	public function mostrarFiltrado($user_id)
	{
		$db = Db::conectar();
		$listaTableros = [];
		$select = $db->prepare('SELECT * FROM tablero where id_tablero in 
			(select id_tablero from tablero_por_usuario where id_user = :id_user)');
		$select->bindValue('id_user', $user_id);
		$select->execute();

		foreach ($select->fetchAll() as $tableros) {
			$myTablero = new tableros();
			$myTablero->setId_tablero($tableros['id_tablero']);
			$myTablero->setNombreTablero($tableros['nombre_tablero']);
			$listaTableros[] = $myTablero;
		}
		return $listaTableros;
	}

	// método para eliminar un tablero, recibe como parámetro el id del tablero
	public function eliminar($id_tablero)
	{
		$db = Db::conectar();
		$eliminar = $db->prepare('DELETE FROM tablero WHERE id_tablero=:id_tablero');
		$eliminar->bindValue('id_tablero', $id_tablero);
		$eliminar->execute();
	}

	// método para buscar un tablero, recibe como parámetro el id del tablero
	public function obtenerTablero($id_tablero)
	{
		$db = Db::conectar();
		$select = $db->prepare('SELECT * FROM tablero WHERE id_tablero=:id_tablero');
		$select->bindValue('id_tablero', $id_tablero);
		$select->execute();
		$tablero = $select->fetch();
		$myTablero = new tableros();
		$myTablero->setId_tablero($tablero['id_tablero']);
		$myTablero->setNombreTablero($tablero['nombre_tablero']);
		return $myTablero;
	}

	// método para actualizar un tablero, recibe como parámetro el tablero
	public function actualizar($tablero)
	{
		$db = Db::conectar();
		$actualizar = $db->prepare('UPDATE tablero SET nombre_tablero=:nombre_tablero WHERE id_tablero=:id_tablero');
		$actualizar->bindValue('id_tablero', $tablero->getId_tablero());
		$actualizar->bindValue('nombre_tablero', $tablero->getNombreTablero());
		$actualizar->execute();
	}

	// método para compartir un tablero, recibe como parámetro el tablero y el mail del usuario
	public function compartirTablero($tablero, $mail_usuario)
	{
		$salida = false;
		$db = Db::conectar();
		$select = $db->prepare('SELECT id_user from usuario where mail = :mail');
		$select->bindValue('mail', $mail_usuario);
		$select->execute();
		$id_usuario = $select->fetch();

		if ($id_usuario['id_user'] != null) {
			$select = $db->prepare('SELECT count(*) AS CONT FROM tablero_por_usuario where id_tablero=:id_tablero
			and id_user = :id_user');
			$select->bindValue('id_tablero', $tablero->getId_tablero());
			$select->bindValue('id_user', $id_usuario['id_user']);
			$select->execute();
			$contador = $select->fetch();

			if ($contador['CONT'] == 0) {

				$compartir = $db->prepare('INSERT into tablero_por_usuario VALUES (NULL, :id_tablero, :id_usuario)');
				$compartir->bindValue('id_tablero', $tablero->getId_tablero());
				$compartir->bindValue('id_usuario', $id_usuario['id_user']);
				$compartir->execute();

				$salida = true;
			}
		}
		return $salida;
	}
}
