<?php
class tareas
{
	private $id_tarea;
	private $fecha_tarea;
	private $desc_tarea;
	private $duracion_tarea;
	private $estado;
	private $id_integrante;
	private $observaciones;
	private $id_tablero;

	function __construct()
	{
	}

	public function getId_tarea()
	{
		return $this->id_tarea;
	}

	public function setId_tarea($id_tarea)
	{
		$this->id_tarea = $id_tarea;
	}

	public function getFecha_tarea()
	{
		return $this->fecha_tarea;
	}

	public function setFecha_tarea($fecha_tarea)
	{
		$this->fecha_tarea = $fecha_tarea;
	}

	public function getDesc_tarea()
	{
		return $this->desc_tarea;
	}

	public function setDesc_tarea($desc_tarea)
	{
		$this->desc_tarea = $desc_tarea;
	}

	public function getDuracion_tarea()
	{
		return $this->duracion_tarea;
	}

	public function setDuracion_tarea($duracion_tarea)
	{
		$this->duracion_tarea = $duracion_tarea;
	}

	public function getEstado()
	{
		return $this->estado;
	}

	public function setEstado($estado)
	{
		$this->estado = $estado;
	}

	public function getId_integrante()
	{
		return $this->id_integrante;
	}

	public function setId_integrante($id_integrante)
	{
		$this->id_integrante = $id_integrante;
	}

	public function getObservaciones()
	{
		return $this->observaciones;
	}

	public function setObservaciones($observaciones)
	{
		$this->observaciones = $observaciones;
	}

	public function getId_tablero()
	{
		return $this->id_tablero;
	}

	public function setId_tablero($id_tablero)
	{
		$this->id_tablero = $id_tablero;
	}
}
