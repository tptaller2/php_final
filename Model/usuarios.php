<?php
	class usuarios{
		private $id_user;
		private $nombre;
		private $apellido;
		private $mail;
		private $password;

		function __construct(){}

		public function getNombre(){
		return $this->nombre;
		}

		public function setNombre($nombre){
			$this->nombre = $nombre;
		}

		public function getApellido(){
			return $this->apellido;
		}

		public function setApellido($apellido){
			$this->apellido = $apellido;
		}

		public function getMail(){
		return $this->mail;
		}

		public function setMail($mail){
			$this->mail = $mail;
		}
		public function getid_user(){
			return $this->id_user;
		}

		public function setid_user($id_user){
			$this->id_user = $id_user;
		}

		public function getPassword(){
			return $this->password;
		}

		public function setPassword($password){
			$this->password = $password;
		}
	}
?>