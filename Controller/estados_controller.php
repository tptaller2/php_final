<?php
require_once('../model/crud_estados.php');
require_once('../model/estados.php');

$crud= new CrudEstados();
$estado= new estados();

	if (isset($_POST['insertar'])) {
		$estado->setDescripcion($_POST['descripcion']);

		$estado->setId_tablero($_POST['id_tablero']);

		$crud->insertar($estado);
		header('Location: ../main.php?id_tablero='.$_POST['id_tablero']);

	}elseif(isset($_POST['actualizar'])){
		$estado->setId_estado($_POST['id_estado']);
		$estado->setDescripcion($_POST['descripcion']);
		$crud->actualizar($estado);
		header('Location: ../main.php?id_tablero='.$_POST['id_tablero']);

	}elseif ($_GET['accion']=='e') {
		$crud->eliminar($_GET['id_estado']);
		header('Location: ../main.php?id_tablero='.$_GET['id_tablero']);

	}elseif($_GET['accion']=='a'){
		header('Location: ../main.php?id_tablero='.$_GET['id_tablero']);
	}
?>