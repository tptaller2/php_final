<?php
require_once('../model/crud_tareas.php');
require_once('../model/Tareas.php');

$crud = new CrudTareas();
$tarea = new tareas();

if (isset($_POST['insertar'])) {
	$tarea->setDesc_tarea($_POST['desc_tarea']);
	$tarea->setFecha_tarea($_POST['fecha_tarea']);
	if ($_POST['estado'] == 0 || $_POST['estado'] == "0") {
		$tarea->setEstado(null);
	} else {
		$tarea->setEstado($_POST['estado']);
	}
	if ($_POST['id_integrante'] == 0 || $_POST['id_integrante'] == "0") {
		$tarea->setId_integrante(null);
	} else {
		$tarea->setId_integrante($_POST['id_integrante']);
	}
	$tarea->setObservaciones($_POST['observaciones']);
	$tarea->setDuracion_tarea($_POST['duracion_tarea']);

	$tarea->setId_tablero($_POST['id_tablero']);

	$crud->insertar($tarea);
	header('Location: ../main.php?id_tablero='.$_POST['id_tablero']);
} elseif (isset($_POST['actualizar'])) {
	$tarea->setId_tarea($_POST['id_tarea']);
	$tarea->setDesc_tarea($_POST['desc_tarea']);
	$tarea->setFecha_tarea($_POST['fecha_tarea']);
	if ($_POST['estado'] == 0 || $_POST['estado'] == "0") {
		$tarea->setEstado(null);
	} else {
		$tarea->setEstado($_POST['estado']);
	}
	if ($_POST['id_integrante'] == 0 || $_POST['id_integrante'] == "0") {
		$tarea->setId_integrante(null);
	} else {
		$tarea->setId_integrante($_POST['id_integrante']);
	}
	$tarea->setObservaciones($_POST['observaciones']);
	$tarea->setDuracion_tarea($_POST['duracion_tarea']);
	$crud->actualizar($tarea);
	header('Location: ../main.php?id_tablero='.$_POST['id_tablero']);
} elseif ($_GET['accion'] == 'e') {
	$crud->eliminar($_GET['id_tarea']);
	header('Location: ../main.php?id_tablero='.$_GET['id_tablero']);
} elseif ($_GET['accion'] == 'a') {
	header('Location: ../main.php?id_tablero='.$_GET['id_tablero']);
}
