<?php
require_once('../model/crud_tableros.php');
require_once('../model/tableros.php');

$crud = new CrudTableros();
$tablero = new tableros();

if (isset($_POST['insertar'])) {
	$tablero->setNombreTablero($_POST['nombre-proyecto']);

	$crud->insertar($tablero, $_POST['user_id']);
	header('Location: ../main.php');
} elseif (isset($_POST['actualizar'])) {
	$tablero->setNombreTablero($_POST['nombre_tablero']);
	$tablero->setId_tablero($_POST['id_tablero']);
	$crud->actualizar($tablero);
	header('Location: ../main.php');
} elseif (isset($_POST['compartir'])) {
	$tablero->setId_tablero($_POST['id_tablero']);

	if ($crud->compartirTablero($tablero, $_POST['mail_usuario'])) {
		echo '<script type="text/javascript">alert("El tablero se compartió correctamente!");
		window.location.href="../main.php"</script>';
	} else {
		echo '<script type="text/javascript">alert("No existe usuario con ese mail o ya fue compartido.");
		window.location.href="../main.php"</script>';
	}
} elseif ($_GET['accion'] == 'e') {
	$crud->eliminar($_GET['id_tablero']);
	header('Location: ../main.php');
} elseif ($_GET['accion'] == 'a') {
	header('Location: ../main.php');
}
?>
