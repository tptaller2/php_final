<?php
require_once('../model/crud_integrantes.php');
require_once('../model/integrantes.php');

$crud= new CrudIntegrantes();
$integrante= new integrantes();

	if (isset($_POST['insertar'])) {
		$integrante->setNombre($_POST['nombre']);
		$integrante->setApellido($_POST['apellido']);
		$integrante->setMail($_POST['mail']);

		$integrante->setId_tablero($_POST['id_tablero']);

		$crud->insertar($integrante);
		header('Location: ../main.php?id_tablero='.$_POST['id_tablero']);

	}elseif(isset($_POST['actualizar'])){
		$integrante->setId_integrante($_POST['id_integrante']);
		$integrante->setNombre($_POST['nombre']);
		$integrante->setApellido($_POST['apellido']);
		$integrante->setMail($_POST['mail']);
		$crud->actualizar($integrante);
		header('Location: ../main.php?id_tablero='.$_POST['id_tablero']);

	}elseif ($_GET['accion']=='e') {
		$crud->eliminar($_GET['id_integrante']);
		header('Location: ../main.php?id_tablero='.$_GET['id_tablero']);

	}elseif($_GET['accion']=='a'){
		header('Location: ../main.php?id_tablero='.$_GET['id_tablero']);
	}
?>